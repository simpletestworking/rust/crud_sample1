#[macro_use]
extern crate rocket;
#[macro_use]
extern crate diesel;
use diesel::{prelude::*, table, Insertable, Queryable};
use rocket::{fairing::AdHoc, serde::json::Json, State};
use rocket_sync_db_pools::database;
use serde::{Deserialize, Serialize};

// DATA STRUCT
#[derive(Serialize, Deserialize)]
struct BlogPost {
    id: i32,
    title: String,
    body: String,
    published: bool,
}

// MAIN FUNCTION
#[launch]
fn rocket() -> _ {
    let rocket= rocket::build();
    
    rocket
      .mount("/", routes![index])
      .mount("/blog-posts", routes![get_all_blog_posts])
}

// ROUTE DECLARATION
// index
#[get("/")]
fn index() -> &'static str {
    "FIRST TEST FOR CRUD APP WITTEN IN RUST & CO."
}

// random
#[get("/random")]
fn get_random_blog_post() -> Json<BlogPost> {
    Json(
        BlogPost {
            id: 1,
            title: "My first post".to_string(),
            body: "This is my first post".to_string(),
            published: true,
        }
    )
}

// get single lement
#[get("/<id>")]
fn get_blog_post(id: i32) -> Json<BlogPost> {
    Json(
      BlogPost {
          id,
          title: "Some title".to_string(),
          body: "Some body".to_string(),
          published: true,
      }
    )
}

// get all books function
#[get("/")]
fn get_all_blog_posts() -> Json<Vec<BlogPost>> {
    Json(vec![
        BlogPost {
            id: 0,
            title: "Harry Potter".to_string(),
            body: "There once lived a boy".to_string(),
            published: true,
        },
        BlogPost {
            id: 1,
            title: "Fantastic Beast".to_string(),
            body: "There once lived a beast".to_string(),
            published: true,
        }
      ]
    )
}


// insert single book 
#[post("/", data = "<blog_post>")]
fn create_blog_post(blog_post: Json<BlogPost>) -> Json<BlogPost> {
    blog_post
}
